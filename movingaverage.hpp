/**
	@file
	MovingAverage implementation. Is based on a circular buffer with a set size. 
	Writing will override the oldest value, which makes it a moving average filter. 
	The data type can be set by <T>, <N> sets the amount of elements, and <B> is the type to be used for a sum of the entire buffer.
	In case of integers, this must hold a value of Tmax * N.
	For example; <T> is a uint8_t with a range from 0-100 and there are 25 elements, <B> must be able to hold 2500, so a uint16_t will suffice.
*/

#pragma once

#include <string.h>

template <typename T, typename B, uint8_t N>
class MovingAverage {
	public:
		/** The normal constructor of the class
		*/
		MovingAverage() :_ptr(_elements), _size(N) {
			reset();
		}
	
		/** The extended constructor of the class
		    This sets all elements to <data>
		*/
		MovingAverage(T data) : _ptr(_elements), _size(N) {
			reset(data);
		}
	
		void reset();
		void reset(T element);
	
		uint8_t getSize() const;
	
		void write(T element);
	
		T average();
		
		T operator()(T element) {
			this->write(element);
			return this->average();
		}
	
	private:
		//Member functions
		T read(uint8_t index);

		//Internal buffer
		T _elements[N];
	
		//Read write pointers
		T *_ptr;
	
		//Size of the buffer
		const uint8_t _size;
};

#include "movingaverage-inl.hpp"