#pragma once

template <typename I>
class _HysteresisBase {
	public:
        _HysteresisBase() = delete;
		_HysteresisBase(I threshold_low, I threshold_high, bool invert = false) : _thres_l(threshold_low), _thres_h(threshold_high), _invert(invert) { }
		
		virtual bool operator()(I value) = 0;
	
	protected:
		//Threshold
		const I _thres_l, _thres_h;
    const bool _invert;
		
		//State
		bool _state_last;
		
		bool process(I value) {
			bool temp = _state_last;
			if (value > _thres_h) { temp = true; }
			if (value < _thres_l) { temp = false; }
			_state_last = temp;
      if (_invert) {
        temp = temp ? false : true;
      }
			return temp;
		}
};

template <typename I>
class Hysteresis : public _HysteresisBase<I> {
	public:
        Hysteresis() = delete;
        Hysteresis(I threshold_low, I threshold_high, bool invert = false) : _HysteresisBase<I>(threshold_low, threshold_high, invert) { }
		
        bool operator()(I value) {
            return this->process(value);
		}
};

template <typename I, typename O>
class HysteresisOutLevel : public _HysteresisBase<I> {
	public:
        HysteresisOutLevel() = delete;
        HysteresisOutLevel(I threshold_low, I threshold_high, O output_low, O output_high, bool invert = false) : _HysteresisBase<I>(threshold_low, threshold_high, invert), _out_l(output_low), _out_h(output_high) { }
		
        O operator()(I value) {
            if(this->process(value)) {
				return _out_h;
			} else {
				return _out_l;
			}
		}
		
	private:
		//Output levels
		const O _out_l, _out_h;
};
