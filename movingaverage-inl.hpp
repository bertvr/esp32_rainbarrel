#pragma once

/** The reset function
    Resets all internal pointers, and clears the data array
*/
template <typename T, typename B, uint8_t N>
void MovingAverage<T, B, N>::reset() {
	_ptr = _elements;
	memset(_elements, 0, _size * sizeof(T));
};

template <typename T, typename B, uint8_t N>
void MovingAverage<T, B, N>::reset(T element) {
	_ptr = _elements;
	memset(_elements, element, _size * sizeof(T));
};

/** This function gets the size of the buffer. This is simply the <_size> variable
*/
template <typename T, typename B, uint8_t N>
uint8_t MovingAverage<T, B, N>::getSize() const {
	return _size;
}

/** This function writes a single element to the buffer. This function will overwrite the oldest element if the buffer is full.
    @param element The element to be written
*/
template <typename T, typename B, uint8_t N>
void MovingAverage<T, B, N>::write(T element) {
	*(_ptr++) = element;
	if (_ptr == _elements + _size) {
		_ptr = _elements;
	}
}

/** Reads a single element from the buffer. This function will NOT remove the element from the buffer that has been read.
    @param index The element to be read
	@return T The read element
*/
template <typename T, typename B, uint8_t N>
T MovingAverage<T, B, N>::read(uint8_t index) {
	return _elements[index];
}

/** Gets the current average of the buffer
	@return T The moving average
*/
template <typename T, typename B, uint8_t N>
T MovingAverage<T, B, N>::average() {
	B temp = B(0); //FIX is this correct way of doing it?

	for (uint8_t i = 0; i < _size; i++) {
		temp += static_cast<B>(read(i));
	}

	return static_cast<T>((temp / _size));
}
