//compile as 'AI Thinker ESP32-CAM'... change to 'DOIT ESP32 DEVKIT V1'
//if onewire lib throws errors when compiling, get the newest version from github

#include "movingaverage.hpp"
#include "hysteresis.hpp"

#include <ArduinoJson.h>
#include <WiFi.h>
#include <NTPClient.h>
#include <WiFiUdp.h>
#include <OneWire.h>
#include <DallasTemperature.h>

const char* ssid = "Familie van Rossem";
const char* password = "biotoop!";

// Set web server port number to 80
WiFiServer server(80);

// Variable to store the HTTP request
String header;

// Assign constants to GPIO pins
const int pin_motor1 = 14;
const int pin_motor2 = 2;
const int pin_led = 4;
const int pin_trig = 15;
const int pin_echo = 13;
const int pin_onewire = 16;

//distance
float distance, distance_raw;
MovingAverage<float, float, 10> filter;
unsigned long measureDistanceMM();
float distanceToLiters(float distance);
uint32_t measurement_time;

//mm 
const float diameter_rainbarrel = 0.611f, area_rainbarrel = pow(diameter_rainbarrel / 2.0f, 2.0f) * M_PI;
const float height_empty = 1.068f;

//temperature
float temp_box, temp_rainbarrel;
const float temp_dump_limit = 2.0f;
OneWire oneWire(pin_onewire);
DallasTemperature temp_sensors(&oneWire);

//NTP
WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP);
uint32_t last_epoch = 0;

//Motor
const float DUMP_THRES = height_empty - 0.8f; //from bottom
const float DUMP_THRES_HYST = 0.05f; //5cm hysterisys
bool dumping = false;
Hysteresis<float> dump_hyst(DUMP_THRES, DUMP_THRES + DUMP_THRES_HYST, true);

void setup() {
  Serial.begin(115200);
  
  // Initialize the output variables as outputs
  pinMode(pin_motor1, OUTPUT);
  pinMode(pin_motor2, OUTPUT);
  pinMode(pin_led, OUTPUT);
  pinMode(pin_trig, OUTPUT);
  pinMode(pin_echo, INPUT);
  
  // Set outputs to LOW
  digitalWrite(pin_motor1, LOW);
  digitalWrite(pin_motor2, LOW);
  digitalWrite(pin_led, LOW);
  digitalWrite(pin_trig, LOW);

  // Connect to Wi-Fi network with SSID and password
  Serial.print("Connecting to ");
  Serial.println(ssid);

  //Set hostname 1
  /*
  bool result = WiFi.setHostname("esp32-rainbarrel");
  if (!result) {
    Serial.println("WiFi hostname couldn't be set: try 1");
  }
  */

  //Begin WiFi
  WiFi.begin(ssid, password);

  //Set hostname 2
  /*
  if (!result) {
    result = WiFi.setHostname("esp32-rainbarrel");
    if (!result) {
      Serial.println("WiFi hostname couldn't be set: try 2");
    }
  }
  */

  //Connect
  bool state = false;
  while (WiFi.status() != WL_CONNECTED) {
    delay(100);
    Serial.print(".");
    digitalWrite(pin_led, state); //toggle led
    state = !state;
  }
  digitalWrite(pin_led, LOW); //shutdown led
  
  // Print local IP address and start web server
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  server.begin();

  //NTP
  timeClient.begin();

  //Temperature
  temp_sensors.begin();

  int amount = temp_sensors.getDeviceCount();
  if (amount != 2) {
    Serial.print("Amount temp sensors found not equal to 2: "); Serial.println(amount);
  }
}

void loop(){
  WiFiClient client = server.available();   // Listen for incoming clients

  //ntp
  while(!timeClient.update()) {
    timeClient.forceUpdate();
  }
  uint32_t epoch = timeClient.getEpochTime();
  if (epoch != last_epoch) {
    //Read distance sensor
    float raw = measureDistanceMM() / 1000.0f;

    if (raw > height_empty) {
      Serial.print("Raw height read too high: "); Serial.println(raw);
    } else {
      distance_raw = raw;
      distance = filter(distance_raw);
    }
    
    //Read temp sensors
    temp_sensors.requestTemperatures();

    temp_rainbarrel = temp_sensors.getTempCByIndex(0);
    temp_box = temp_sensors.getTempCByIndex(1);
    
    //Time
    measurement_time = epoch;

    //Check dump
    dumping = dump_hyst(distance);
    if (temp_rainbarrel < temp_dump_limit) {
      dumping = false;
    }
    dump(dumping);

    Serial.print("distance | raw: "); Serial.print(distance_raw); Serial.print("\tfilt: "); Serial.println(distance);
    Serial.print("Temp box: "); Serial.print(temp_box); Serial.print("\tTemp rainbarrel: "); Serial.println(temp_rainbarrel);
    Serial.println(' ');
  }
  last_epoch = epoch;
  

  if (client) {
    Serial.println("New Client");
    String currentLine = "";
    while (client.connected()) {
      if (client.available()) {
        char c = client.read();
        header += c;
        if (c == '\n') {
          // if the current line is blank, you got two newline characters in a row.
          // that's the end of the client HTTP request, so send a response:
          if (currentLine.length() == 0) {
            // HTTP headers always start with a response code (e.g. HTTP/1.1 200 OK)
            // and a content-type so the client knows what's coming, then a blank line:
            client.println("HTTP/1.1 200 OK");
            //client.println("Content-type:text/html"); //for text
            client.println("Content-type:application/json"); //application/json
            client.println("Connection: close");
            client.println();

            //Display JSON
            const int capacity = JSON_OBJECT_SIZE(7);
            StaticJsonDocument<capacity> doc;
            doc["distance_m"] = distance;
            doc["distance_raw_m"] = distance_raw;
            doc["temp_box"] = temp_box;
            doc["temp_rainbarrel"] = temp_rainbarrel;
            doc["liters"] = distanceToLiters(distance);
            doc["dumping"] = dumping;
            doc["measurement_time"] = measurement_time;
            serializeJsonPretty(doc, client);
            
            // The HTTP response ends with another blank line
            client.println();
            // Break out of the while loop
            break;
          } else { // if you got a newline, then clear currentLine
            currentLine = "";
          }
        } else if (c != '\r') {  // if you got anything else but a carriage return character,
          currentLine += c;      // add it to the end of the currentLine
        }
      }
    }
    // Clear the header variable
    header = "";
    
    // Close the connection
    client.stop();
    //Serial.println("Client disconnected");
    //Serial.println("");
  }
}

//function blocks up to 70ms
unsigned long measureDistanceMM() {
  //Send trigger
  digitalWrite(pin_trig, HIGH);
  delayMicroseconds(11);
  digitalWrite(pin_trig, LOW);
  
  //Receive echo
  unsigned long duration = pulseIn(pin_echo, HIGH, 70000); //60ms max timeout -> 70 for buffer
  return (unsigned long) (((float) duration / 58.0) * 10.0);
}

//Assume perfect cylinder
float distanceToLiters(float distance) {
  //Check if proper reading
  if (distance > height_empty) {
    //Water lower than bottom..
    Serial.print("Cannot compute liters: distance too big -> "); Serial.println(distance);
    return -1.0f;
  }
  
  //Compute
  float water_height = height_empty - distance;
  float liters = 1000.0f * (area_rainbarrel * water_height);
  return liters;
}

void dump(bool state) {
  digitalWrite(pin_motor1, state);
}

void printAddress(DeviceAddress deviceAddress)
{
  Serial.print("Device address: ");
  for (uint8_t i = 0; i < 8; i++)
  {
    // zero pad the address if necessary
    if (deviceAddress[i] < 16) Serial.print("0");
    Serial.print(deviceAddress[i], HEX);
  }
  Serial.println(' ');
}
